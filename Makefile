all: container
container:
	@docker build -t alexsapran/example-cli .

usage:
	@echo "Usage : docker run -it --rm alexsapran/example-cli -e alex"
	@echo "Show help: docker run -it --rm alexsapran/example-cli --help"

clean:
	@docker rmi alexsapran/example-cli

