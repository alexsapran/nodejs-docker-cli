#!/usr/bin/env node
'use strict';
/**
 * Require dependencies
 *
 */
const program = require('commander'),
  chalk = require("chalk"),
  exec = require('child_process').exec,
  pkg = require('./package.json'),
  net = require("net");


program
  .version(pkg.version)
  .usage('<options>')
  .option('-e, --example [e]','Print back whatever was in the input');
program.parse(process.argv);


console.log("Application started");
if ( !(program.example) ){
  program.help();
}

if (program.example) {
  console.log("Got input "+program.example);
}

console.log("Execution finished")
